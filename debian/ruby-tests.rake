require "rake/testtask"

desc "Run all the tests"
task default: :test

Rake::TestTask.new(:test) do |t|
  t.libs << "test" << "test/lib"
  t.libs << "lib"
  t.test_files = FileList['test/**/test_*.rb']
end

task :default => :test
